# lottery
This is software UI framework objective-c code.

screenshots
-----------------------
<img alt="one" src="https://raw.github.com/charsdavy/lottery-iphone/master/screenshots/lottery1.jpg" width="180">
&nbsp;&nbsp;
<img alt="two" src="https://raw.github.com/charsdavy/lottery-iphone/master/screenshots/lottery2.jpg" width="180">
&nbsp;&nbsp;
<img alt="three" src="https://raw.github.com/charsdavy/lottery-iphone/master/screenshots/lottery3.jpg" width="180">
&nbsp;&nbsp;
<img alt="four" src="https://raw.github.com/charsdavy/lottery-iphone/master/screenshots/lottery4.jpg" width="180">
&nbsp;&nbsp;
<img alt="five" src="https://raw.github.com/charsdavy/lottery-iphone/master/screenshots/lottery5.jpg" width="180">
&nbsp;&nbsp;
<img alt="six" src="https://raw.github.com/charsdavy/lottery-iphone/master/screenshots/lottery6.jpg" width="180">
&nbsp;&nbsp;
<img alt="seven" src="https://raw.github.com/charsdavy/lottery-iphone/master/screenshots/lottery7.jpg" width="180">
&nbsp;&nbsp;
<img alt="eight" src="https://raw.github.com/charsdavy/lottery-iphone/master/screenshots/lottery8.jpg" width="180">
&nbsp;&nbsp;

#Blog
My blog is [here](http://www.cnblogs.com/chars). Welcome to visit!

Copyright